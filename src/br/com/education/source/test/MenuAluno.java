
package br.com.education.source.test;

import br.com.education.source.facade.Facade;
import br.com.education.source.model.Aluno;
import br.com.education.source.model.Endereco;
import java.util.Scanner;
import br.com.education.source.util.DateUtil;
import br.com.education.source.util.MensagensUtil;
import java.text.ParseException;
import java.util.List;

public class MenuAluno {
    
    public static void main(String[] args) throws ParseException, Exception {
        
        Scanner entrada = new Scanner(System.in);
        Facade facade = new Facade();
        Aluno aluno;
        Long id;
        
        String opcao;
        
        do {
            System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CABECALHO_MENU));
            System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CADASTRAR));
            System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_EDITAR));
            System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_REMOVER));
            System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CONSULTAR));
            System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_LISTAR));
            System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_SAIR));
            
            opcao = entrada.nextLine();
            
            switch(opcao){
                case "1":
                    
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CABECALHO_OPCAO1));
                    aluno = new Aluno();
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_NOME));
                    aluno.setNome(entrada.nextLine());
                    
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_EMAIL));
                    aluno.setEmail(entrada.nextLine());
                    
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CABECALHO_ENDERECO));
                    Endereco end = new Endereco();
                    
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_NOME_RUA));
                    end.setRua(entrada.nextLine());
                    
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_NOME_BAIRRO));
                    end.setBairro(entrada.nextLine());
                    
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CEP));
                    end.setCep(entrada.nextLine());
                    
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CIDADE));
                    end.setCidade(entrada.nextLine());
                    
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_ESTADO));
                    end.setEstado(entrada.nextLine());
                    
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_COMPLEMENTO));
                    end.setComplemento(entrada.nextLine());
                    aluno.setEnd(end);
                    
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_GENERO));
                    aluno.setGenero(entrada.nextLine());
                    
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_MATRICULA));
                    aluno.setMatricula(entrada.nextLine());
                    
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_NOME_MAE));
                    aluno.setNomeDaMae(entrada.nextLine());
                    
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_NOME_PAI));
                    aluno.setNomeDoPai(entrada.nextLine());
                    
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_RESPONSAVEL));
                    aluno.setResponsavel(entrada.nextLine());
                    
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_SENHA));
                    aluno.setSenha(entrada.nextLine());
                    
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_DATA_NASCIMENTO));
                    aluno.setDataNascimento(DateUtil.stringToDate(entrada.nextLine()));
                    
                    facade.salvarAluno(aluno);
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_SUCESSO_CASDASTRAR_ALUNO));
                    break;
                case "2":
                    
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CABECALHO_OPCAO2));
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_INFORME_ID));
                    id = Long.parseLong(entrada.nextLine());
                    aluno = facade.getIDAluno(id);
                    
                    if(aluno != null){
                        
                        System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_NOVO_NOME));
                        aluno.setNome(entrada.nextLine());
                        
                        System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_NOVO_EMAIL));
                        aluno.setEmail(entrada.nextLine());           
                        
                        facade.editarAluno(aluno);
                        
                        System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_SUCESSO_EDITAR));
                    }else{
                        System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_ERRO_ALUNO_NAO_ENCONTRADO));
                    }
                    break;
                case "3":
                    
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CABECALHO_OPCAO3));
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_INFORME_ID_EXCLUIRALUNO));
                    id = Long.parseLong(entrada.nextLine());
                    aluno = facade.getIDAluno(id);
                    
                    if(aluno != null){           
                        
                        facade.removerAluno(aluno);
                        
                        System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_SUCESSO_EXCLUIR));
                    }else{
                        System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_ERRO_ALUNO_NAO_ENCONTRADO));
                    }

                    break;
                case "4":
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CABECALHO_OPCAO4));

                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_MATRICULAALUNO));
                    String matricula = entrada.nextLine();

                    List<Aluno> listaProf = facade.buscarAluno(matricula);
                    if (!listaProf.isEmpty()) {
                        listaProf.forEach((p) -> {
                            System.out.println(p);
                        });

                    } else {
                        System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_ERRO_ALUNO_NAO_ENCONTRADO));
                    }
                    break;
                case "5":
                    
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CABECALHO_OPCAO5));

                    for (Aluno a : facade.listarAluno()) {
                        System.out.println(a);
                    }
                    break;
                case "6":
                    System.exit(0);
                    break;
                default:
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MEU_OPCAO_INVALIDA));
                    break;
            }
            
        } while (!opcao.equals("6"));
    }
}
        