/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.education.source.test;

import br.com.education.source.facade.Facade;
import br.com.education.source.model.Endereco;
import br.com.education.source.model.Professor;
import br.com.education.source.util.DateUtil;
import br.com.education.source.util.MensagensUtil;
import java.text.ParseException;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author arthu
 */
public class MenuTestes {

    public static void main(String[] args) throws ParseException, Exception {

        //ler dados do usuário
        Scanner leia = new Scanner(System.in);
        Facade facade = new Facade();
        Professor professor;
        Long id;

        String opcao;

        do {
            System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CABECALHO_MENU));
            System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CADASTRAR));
            System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_EDITAR));
            System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_REMOVER));
            System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CONSULTAR));
            System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_LISTAR));
            System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_SAIR));

            opcao = leia.nextLine();

            switch (opcao) {
                case "1":

                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CABECALHO_OPCAO1));
                    professor = new Professor();
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_NOME));
                    professor.setNome(leia.nextLine());

                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CPF_PROFESSOR));
                    professor.setCpf(leia.nextLine());

                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_EMAIL));
                    professor.setEmail(leia.nextLine());

                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_GENERO));
                    professor.setGenero(leia.nextLine());

                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_DATA_NASCIMENTO));
                    professor.setDataNascimento(DateUtil.stringToDate(leia.nextLine()));

                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_SENHA));
                    professor.setSenha(leia.nextLine());

                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CABECALHO_ENDERECO));
                    Endereco end = new Endereco();

                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_NOME_RUA));
                    end.setRua(leia.nextLine());

                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_NOME_BAIRRO));
                    end.setBairro(leia.nextLine());

                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CEP));
                    end.setCep(leia.nextLine());

                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CIDADE));
                    end.setCidade(leia.nextLine());

                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_ESTADO));
                    end.setEstado(leia.nextLine());

                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_COMPLEMENTO));
                    end.setComplemento(leia.nextLine());

                    professor.setEnd(end);

                    facade.salvarProfessor(professor);
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_SUCESSO_CASDASTRAR_PROFESSOR));
                    break;
                case "2":

                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CABECALHO_OPCAO2));
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_INFORME_ID));
                    id = Long.parseLong(leia.nextLine());

                    professor = facade.getIDProfessor(id);
                    if (professor != null) {
                        System.out.println("Antigo e-mail: (" + professor.getEmail() + ")");
                        System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_NOVO_EMAIL));
                        professor.setEmail(leia.nextLine());
                        facade.editarProfessor(professor);
                        System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_SUCESSO_EDITAR));
                    } else {
                        System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_ERRO_PROFESSOR_NAO_ENCONTRADO));
                    }
                    break;
                case "3":
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CABECALHO_OPCAO3));
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_INFORME_ID_EXCLUIRPROFESSOR));
                    id = Long.parseLong(leia.nextLine());
                    professor = facade.getIDProfessor(id);
                    if (professor != null) {

                        facade.removerProfessor(professor);
                        System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_SUCESSO_EXCLUIR));
                    } else {
                        System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_ERRO_PROFESSOR_NAO_ENCONTRADO));
                    }
                    break;
                case "4":
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CABECALHO_OPCAO4));

                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CPF_PROFESSOR));
                    String cpf = leia.nextLine();

                    List<Professor> listaProf = facade.buscarProfessor(cpf);
                    if (!listaProf.isEmpty()) {
                        listaProf.forEach((p) -> {
                            System.out.println(p);
                        });

                    } else {
                        System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_ERRO_PROFESSOR_NAO_ENCONTRADO));
                    }
                    break;
                case "5":

                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MENU_CABECALHO_OPCAO5));

                    for (Professor p : facade.listarProfessor()) {
                        System.out.println(p);
                    }
                    break;
                case "6":
                    
                    System.exit(0);
                    break;
                default:
                    System.out.println(MensagensUtil.getString(MensagensUtil.MSG_MEU_OPCAO_INVALIDA));
                    break;
            }

        } while (!opcao.equals("6"));
    }
}
