
package br.com.education.source.facade;

import br.com.education.source.dao.AlunoDAO;
import br.com.education.source.dao.ProfessorDAO;
import br.com.education.source.model.Aluno;
import br.com.education.source.model.Professor;
import java.util.List;

/**
 *
 * @author reina
 */
public class Facade {
    
    private final ProfessorDAO professorDAO = new ProfessorDAO();
    
    private final AlunoDAO alunoDao = new AlunoDAO();
    
    public void salvarAluno(Aluno aluno) throws Exception{
        alunoDao.salvar(aluno);
    }
    public void editarAluno(Aluno aluno) throws Exception{
        alunoDao.editar(aluno);
    }
    public void removerAluno(Aluno aluno) throws Exception{
        alunoDao.remover(aluno);
    }
    public List<Aluno> buscarAluno(String matricula) throws Exception{
        return alunoDao.buscar(matricula);
    }
    public List<Aluno> listarAluno() throws Exception{
        return alunoDao.listar();
    }
    public Aluno getIDAluno(Long ID) throws Exception{
        return alunoDao.getPorId(ID);
    }
    
    public void salvarProfessor(Professor professor) throws Exception{
        professorDAO.salvar(professor);
    }
    
    public void editarProfessor(Professor professor) throws Exception{
        professorDAO.editar(professor);
    }
    public void removerProfessor(Professor professor) throws Exception{
        professorDAO.remover(professor);
    }
    public List<Professor> buscarProfessor(String cpf) throws Exception{
        return professorDAO.buscar(cpf);
    }
    public List<Professor> listarProfessor() throws Exception{
        return professorDAO.listar();
    }
    public Professor getIDProfessor(Long ID) throws Exception{
        return professorDAO.getPorId(ID);
    }
    
}
