package br.com.education.source.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    
    private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("dd/MM/yyyy");
    private static Date instance;
    
    private DateUtil(){
        
    }
    
    public static Date getInstance(){
        if(instance == null){
            instance = new Date();
        }
        return instance;
    }
    
    public static Date stringToDate(String data) throws ParseException{
        return FORMATTER.parse(data);
    }
    
    public static String dateToString(Date data){
        if(data != null ){
            return FORMATTER.format(data);
        }
        return "Data não informada";
    }
    
}
