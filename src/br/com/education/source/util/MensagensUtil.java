package br.com.education.source.util;

import java.util.Locale;
import java.util.ResourceBundle;

public class MensagensUtil {
    
    public static final Locale PT_BR = new Locale("pt", "BR");
    public static final Locale EN_US = new Locale("en", "US");
    
    
    
    private static ResourceBundle resource = ResourceBundle.getBundle("mensagens", PT_BR);
    
    public static final String MSG_ADM_ERRO_SALVAR = "msg.adm.erro.salvar";
    public static final String MSG_ADM_ERRO_EDITAR = "msg.adm.erro.editar ";
    public static final String MSG_ADM_ERRO_REMOVER = "msg.adm.erro.remover";     
    public static final String MSG_ADM_ERRO_LISTAR = "msg.adm.erro.listar";
    public static final String MSG_ADM_ERRO_BUSCAR = "msg.adm.erro.buscar" ;   
    public static final String MSG_ADM_ERRO_GETPORID = "msg.adm.erro.getPorID";       
    public static final String MSG_ALU_ERRO_SALVAR = "msg.alu.erro.salvar ";   
    public static final String MSG_ALU_ERRO_EDITAR = "msg.alu.erro.editar";     
    public static final String MSG_ALU_ERRO_REMOVER = "msg.alu.erro.remover";    
    public static final String MSG_ALU_ERRO_LISTAR = "msg.alu.erro.listar";   
    public static final String MSG_ALU_ERRO_BUSCAR = "msg.alu.erro.buscar";      
    public static final String MSG_ALU_ERRO_GETPORID = "msg.alu.erro.getPorID";       
    public static final String MSG_NOTA_ERRO_SALVAR = "msg.nota.erro.salvar";      
    public static final String MSG_NOTA_ERRO_EDITAR = "msg.nota.erro.editar";      
    public static final String MSG_NOTA_ERRO_REMOVER = "msg.nota.erro.remover"; 
    public static final String MSG_NOTA_ERRO_LISTAR = "msg.nota.erro.listar";
    public static final String MSG_NOTA_ERRO_BUSCAR = "msg.nota.erro.buscar";       
    public static final String MSG_NOTA_ERRO_GETPORID = "msg.nota.erro.getPorID";       
    public static final String MSG_PROF_ERRO_SALVAR = "msg.prof.erro.salvar";     
    public static final String MSG_PROF_ERRO_EDITAR = "msg.prof.erro.editar";     
    public static final String MSG_PROF_ERRO_REMOVER = "msg.prof.erro.remover";       
    public static final String MSG_PROF_ERRO_LISTAR = "msg.prof.erro.listar";       
    public static final String MSG_PROF_ERRO_BUSCAR = "msg.prof.erro.buscar";       
    public static final String MSG_PROF_ERRO_GETPORID = "msg.prof.erro.getPorID";         
    public static final String MSG_SER_ERRO_SALVAR = "msg.ser.erro.salvar";
    public static final String MSG_SER_ERRO_EDITAR ="msg.ser.erro.editar";
    public static final String MSG_SER_ERRO_REMOVER ="msg.ser.erro.remover";
    public static final String MSG_SER_ERRO_LISTAR ="msg.ser.erro.listar";
    public static final String MSG_SER_ERRO_BUSCAR ="msg.ser.erro.buscar";
    public static final String MSG_SER_ERRO_GETPORID = "msg.ser.erro.getPorID";
    public static final String MSG_DISC_ERRO_SALVAR ="msg.disc.erro.salvar";
    public static final String MSG_DISC_ERRO_EDITAR ="msg.disc.erro.editar";
    public static final String MSG_DISC_ERRO_REMOVER ="msg.disc.erro.remover";
    public static final String MSG_DISC_ERRO_LISTAR ="msg.disc.erro.listar";
    public static final String MSG_DISC_ERRO_BUSCAR ="msg.disc.erro.buscar";
    public static final String MSG_DISC_ERRO_GETPORID ="msg.disc.erro.getPorID";
    public static final String MSG_MENU_CABECALHO_MENU ="msg.menu.cabecalho.menu";
    public static final String MSG_MENU_CADASTRAR ="msg.menu.cadastrar";
    public static final String MSG_MENU_EDITAR ="msg.menu.editar";
    public static final String MSG_MENU_REMOVER ="msg.menu.remover";
    public static final String MSG_MENU_CONSULTAR = "msg.menu.consultar";
    public static final String MSG_MENU_LISTAR = "msg.menu.listar";
    public static final String MSG_MENU_SAIR ="msg.menu.sair";
    
    public static final String MSG_MENU_CABECALHO_OPCAO1 = "msg.menu.cabecalho.opcao1";   
    public static final String MSG_MENU_CABECALHO_ENDERECO = "msg.menu.cabecalho.endereco";     
    public static final String MSG_MENU_CABECALHO_OPCAO2 = "msg.menu.cabecalho.opcao2";     
    public static final String MSG_MENU_CABECALHO_OPCAO3 = "msg.menu.cabecalho.opcao3";   
    public static final String MSG_MENU_CABECALHO_OPCAO4 = "msg.menu.cabecalho.opcao4";      
    public static final String MSG_MENU_CABECALHO_OPCAO5 = "msg.menu.cabecalho.opcao5";      
    public static final String MSG_MEU_OPCAO_INVALIDA = "msg.meu.opcao_invalida";      
    public static final String MSG_MENU_NOME = "msg.menu.nome";
    public static final String MSG_MENU_EMAIL = "msg.menu.email";
    public static final String MSG_MENU_NOME_RUA = "msg.menu.nome_rua";    
    public static final String MSG_MENU_NOME_BAIRRO = "msg.menu.nome_bairro";       
    public static final String MSG_MENU_CIDADE = "msg.menu.cidade";
    public static final String MSG_MENU_CEP = "msg.menu.cep";
    public static final String MSG_MENU_ESTADO = "msg.menu.estado";
    public static final String MSG_MENU_COMPLEMENTO = "msg.menu.complemento";     
    public static final String MSG_MENU_GENERO = "msg.menu.genero";      
    public static final String MSG_MENU_MATRICULA = "msg.menu.matricula";     
    public static final String MSG_MENU_NOME_MAE = "msg.menu.nome_mae" ;      
    public static final String MSG_MENU_NOME_PAI = "msg.menu.nome_pai";    
    public static final String MSG_MENU_RESPONSAVEL = "msg.menu.responsavel";       
    public static final String MSG_MENU_SENHA = "msg.menu.senha";      
    public static final String MSG_MENU_DATA_NASCIMENTO = "msg.menu.data_nascimento" ;      
    public static final String MSG_MENU_SUCESSO_CASDASTRAR_ALUNO = "msg.menu.sucesso.casdastrar_aluno";    
    public static final String MSG_MENU_SUCESSO_CASDASTRAR_PROFESSOR = "msg.menu.sucesso.casdastrar_Professor";    
    public static final String MSG_MENU_INFORME_ID = "msg.menu.informe_id";
    public static final String MSG_MENU_INFORME_ID_EXCLUIRALUNO = "msg.menu.informe_id_excluirAluno";    
    public static final String MSG_MENU_INFORME_ID_EXCLUIRPROFESSOR = "msg.menu.informe_id_excluirProfessor";  
    public static final String MSG_MENU_NOVO_NOME = "msg.menu.novo_nome";
    public static final String MSG_MENU_NOVO_EMAIL = "msg.menu.novo_email";
    public static final String MSG_MENU_SUCESSO_EDITAR = "msg.menu.sucesso.editar";
    public static final String MSG_MENU_ERRO_ALUNO_NAO_ENCONTRADO = "msg.menu.erro.aluno_nao_encontrado";     
    public static final String MSG_MENU_ERRO_PROFESSOR_NAO_ENCONTRADO = "msg.menu.erro.professor_nao_encontrado";   
    public static final String MSG_MENU_SUCESSO_EXCLUIR = "msg.menu.sucesso.excluir";      
    public static final String MSG_MENU_CPF_PROFESSOR = "msg.menu.cpfProfessor";   
    public static final String MSG_MENU_MATRICULAALUNO = "msg.menu.matriculaAluno";    
    public static final String MSG_MENU_ERRO_CONSULTARALUNO = "msg.menu.erro.consultarAluno";    
    public static final String MSG_MENU_ERRO_CONSULTARPROFESSOR = "msg.menu.erro.consultarProfessor"; 
   
    public static String getString(String chave) {
        return resource.getString(chave);
    }

    public static void setLocale(Locale locale) {
        resource = ResourceBundle.getBundle("mensagens", locale);
    }
}
