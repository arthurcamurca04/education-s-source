/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.education.source.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author arthu
 */
public class PersistenceUtil {

    private static final EntityManagerFactory FACTORY
            = Persistence.createEntityManagerFactory("Education_s_SourcePU");

    public static EntityManager criarEntidade() {
        return FACTORY.createEntityManager();
    }
}
