/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.education.source.dao;

import br.com.education.source.model.Professor;
import br.com.education.source.util.MensagensUtil;
import br.com.education.source.util.PersistenceUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author arthu
 */
public class ProfessorDAO implements IDAO<Professor>{
    private EntityManager em;
    @Override
    public void salvar(Professor professor) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            em.getTransaction().begin();
            em.persist(professor);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_PROF_ERRO_SALVAR));
        }
    }

    @Override
    public void editar(Professor professor) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            em.getTransaction().begin();
            em.merge(professor);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_PROF_ERRO_EDITAR));
            
        }finally{
            em.close();
        }
    }

    @Override
    public void remover(Professor professor) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            em.getTransaction().begin();
            em.remove(em.getReference(Professor.class, professor.getId()));
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_PROF_ERRO_REMOVER));
            
        }finally{
            em.close();
        }
           
    }

    @Override
    public List<Professor> listar() throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            Query query = em.createQuery("SELECT p FROM Professor p");
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_PROF_ERRO_LISTAR));
        }finally{
            em.close();
        }
    }

    @Override
    public List<Professor> buscar(String cpf) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            Query query = em.createQuery("SELECT p FROM Professor p WHERE p.cpf LIKE :p");
            query.setParameter("p", cpf + "%");
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_PROF_ERRO_BUSCAR));
        }finally{
            em.close();
        }
    }

    @Override
    public Professor getPorId(Long id) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            return em.find(Professor.class, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_PROF_ERRO_GETPORID));
    
        }finally{
            em.close();
        }
    }
    
    
}
