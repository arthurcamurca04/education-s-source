/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.education.source.dao;

import br.com.education.source.model.Nota;
import br.com.education.source.model.Serie;
import br.com.education.source.util.MensagensUtil;
import br.com.education.source.util.PersistenceUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author arthu
 */
public class NotaDAO implements IDAO<Nota>{
    
    private EntityManager em;
    @Override
    public void salvar(Nota nota) throws Exception  {
        try {
            em = PersistenceUtil.criarEntidade();
            em.getTransaction().begin();
            em.persist(nota);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_NOTA_ERRO_SALVAR));
        } em.close();
    }

    @Override
    public void editar(Nota nota) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            em.getTransaction().begin();
            em.merge(nota);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_NOTA_ERRO_EDITAR));
            
        }finally{
            em.close();
        }
    }

    @Override
    public void remover(Nota nota) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            em.getTransaction().begin();
            em.remove(em.getReference(Nota.class, nota.getId()));
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_NOTA_ERRO_REMOVER));
            
        }finally{
            em.close();
        }
    }

    @Override
    public List<Nota> listar() throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            Query query = em.createQuery("SELECT p FROM Nota p");
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_NOTA_ERRO_LISTAR));
        }finally{
            em.close();
        }
    }

    @Override
    public List<Nota> buscar(String busca) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Nota getPorId(Long id) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            return em.find(Nota.class, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new UnsupportedOperationException(MensagensUtil.getString(MensagensUtil.MSG_NOTA_ERRO_GETPORID)); 
        }finally{
            em.close();
        }
    }
}
    

