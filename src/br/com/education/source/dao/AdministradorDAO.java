/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.education.source.dao;

import br.com.education.source.model.Administrador;
import br.com.education.source.util.MensagensUtil;
import br.com.education.source.util.PersistenceUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author arthu
 */
public class AdministradorDAO implements IDAO<Administrador>{

    private EntityManager em;
    @Override
    public void salvar(Administrador adm) throws Exception  {
        try {
            em = PersistenceUtil.criarEntidade();
            em.getTransaction().begin();
            em.persist(adm);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_ADM_ERRO_SALVAR));
        } em.close();
    }

    @Override
    public void editar(Administrador administrador) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            em.getTransaction().begin();
            em.merge(administrador);
            em.getTransaction().commit();
        }catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_ADM_ERRO_EDITAR));
        }finally{
            em.close();
        }
    }

    @Override
    public void remover(Administrador administrador) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            em.getTransaction().begin();
            em.remove(administrador);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_ADM_ERRO_REMOVER));
            
        }finally{
            em.close();
        }
    }

    @Override
    public List<Administrador> listar() throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            Query query = em.createQuery("SELECT a FROM Administrador a");
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_ADM_ERRO_SALVAR));
        }finally{
            em.close();
        }
    }

    @Override
    public List<Administrador> buscar(String cpfAdmin) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            Query query = em.createQuery("SELECT a FROM Administrador a WHERE a.cpfAdmin LIKE :p");
            query.setParameter("p", cpfAdmin + "%");
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_ADM_ERRO_BUSCAR));
        }finally{
            em.close();
        }
    }

    @Override
    public Administrador getPorId(Long id) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            return em.find(Administrador.class, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_ADM_ERRO_GETPORID));
        }finally{
            em.close();
        }
    }      
}
