
package br.com.education.source.dao;

import br.com.education.source.model.Disciplina;
import br.com.education.source.util.MensagensUtil;
import br.com.education.source.util.PersistenceUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author Reina
 */
public class DiciplinaDAO implements IDAO<Disciplina>{

    private EntityManager em;
    
    @Override
    public void salvar(Disciplina diciplina) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            em.getTransaction().begin();
            em.persist(diciplina);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_DISC_ERRO_SALVAR));
        } em.close();
    }

    @Override
    public void editar(Disciplina diciplina) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            em.getTransaction().begin();
            em.merge(diciplina);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_DISC_ERRO_EDITAR));
            
        }finally{
            em.close();
        }
    }

    @Override
    public void remover(Disciplina disciplina) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            em.getTransaction().begin();
            em.remove(em.getReference(Disciplina.class, disciplina.getCodDisc()));
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_DISC_ERRO_REMOVER));
            
        }finally{
            em.close();
        }
    }

    @Override
    public List<Disciplina> listar() throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            Query query = em.createQuery("SELECT p FROM Disciplina p");
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_DISC_ERRO_LISTAR));
        }finally{
            em.close();
        }
    }

    @Override
    public List<Disciplina> buscar(String codDisc) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            Query query = em.createQuery("SELECT p FROM Disciplina p WHERE p.codDisc LIKE :p");
            query.setParameter("p", codDisc + "%");
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_DISC_ERRO_BUSCAR));
        }finally{
            em.close();
        }
    }

    @Override
    public Disciplina getPorId(Long id) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            return em.find(Disciplina.class, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new UnsupportedOperationException(MensagensUtil.getString(MensagensUtil.MSG_DISC_ERRO_GETPORID)); 
        }finally{
            em.close();
        }
    }
    
}
