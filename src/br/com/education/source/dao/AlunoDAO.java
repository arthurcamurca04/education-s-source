package br.com.education.source.dao;

import br.com.education.source.model.Aluno;
import br.com.education.source.util.MensagensUtil;
import br.com.education.source.util.PersistenceUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public class AlunoDAO implements IDAO<Aluno> {   
    private EntityManager em;

    @Override
    public void salvar(Aluno aluno) throws Exception  {
        try {
            em = PersistenceUtil.criarEntidade();
            em.getTransaction().begin();
            em.persist(aluno);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_ALU_ERRO_SALVAR));
        } em.close();
    }

    @Override
    public void editar(Aluno aluno) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            em.getTransaction().begin();
            em.merge(aluno);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_ALU_ERRO_EDITAR));
            
        }finally{
            em.close();
        }
    }

    @Override
    public void remover(Aluno aluno) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            em.getTransaction().begin();
            em.remove(em.getReference(aluno.getClass(), aluno.getId()));
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_ALU_ERRO_REMOVER));
            
        }finally{
            em.close();
        }
    }

    @Override
    public List<Aluno> listar() throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            Query query = em.createQuery("SELECT a FROM Aluno a");
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_ALU_ERRO_LISTAR));
        }finally{
            em.close();
        }
    }

    @Override
    public List<Aluno> buscar(String matricula) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            Query query = em.createQuery("SELECT a FROM Aluno a WHERE a.matricula LIKE :p");
            query.setParameter("p", matricula + "%");
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_ALU_ERRO_BUSCAR));
        }finally{
            em.close();
        }
    }

    @Override
    public Aluno getPorId(Long id) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            return em.find(Aluno.class, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_ALU_ERRO_GETPORID));
        }finally{
            em.close();
        }
    }

}
