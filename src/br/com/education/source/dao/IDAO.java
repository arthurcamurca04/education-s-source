package br.com.education.source.dao;

import java.util.List;

public interface IDAO<T> {
    public void salvar(T t) throws Exception;
    
     public void editar(T t) throws Exception;

    public void remover(T t) throws Exception;

    public List<T> listar() throws Exception;

    public List<T> buscar(String busca) throws Exception;

    public T getPorId(Long id) throws Exception;
}
