/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.education.source.dao;

import br.com.education.source.model.Serie;
import br.com.education.source.util.MensagensUtil;
import br.com.education.source.util.PersistenceUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author arthu
 */
public class SerieDAO implements IDAO<Serie>{

    private EntityManager em;
    @Override
    public void salvar(Serie serie) throws Exception  {
        try {
            em = PersistenceUtil.criarEntidade();
            em.getTransaction().begin();
            em.persist(serie);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
            throw new Exception(MensagensUtil.getString(MensagensUtil.MSG_SER_ERRO_SALVAR));
        }finally{
            em.close();
        }
    }

    @Override
    public void editar(Serie serie) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            em.getTransaction().begin();
            em.merge(serie);
            em.getTransaction().commit();
            
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
            throw new UnsupportedOperationException(MensagensUtil.getString(MensagensUtil.MSG_SER_ERRO_EDITAR)); 
        }finally{
            em.close();
        }
    }

    @Override
    public void remover(Serie serie) throws Exception {
           try {
            em = PersistenceUtil.criarEntidade();
            em.getTransaction().begin();
            em.remove(serie);
            em.getTransaction().commit();
            
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
            throw new UnsupportedOperationException(MensagensUtil.getString(MensagensUtil.MSG_SER_ERRO_REMOVER)); 
        }finally{
               em.close();
           }
    }

    @Override
    public List<Serie> listar() throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            Query query = em.createQuery("SELECT s FROM Serie s");
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            throw new UnsupportedOperationException(MensagensUtil.getString(MensagensUtil.MSG_SER_ERRO_LISTAR)); 
        }finally{
            em.close();
        }
    }

    @Override
    public List<Serie> buscar(String busca) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            Query query = em.createQuery("SELECT s FROM Serie s WHERE s.nomeTurma LIKE :p");
            query.setParameter("p", busca + "%");
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            throw new UnsupportedOperationException(MensagensUtil.getString(MensagensUtil.MSG_SER_ERRO_BUSCAR)); 
        }finally{
            em.close();
        }
    }

    @Override
    public Serie getPorId(Long id) throws Exception {
        try {
            em = PersistenceUtil.criarEntidade();
            return em.find(Serie.class, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new UnsupportedOperationException(MensagensUtil.getString(MensagensUtil.MSG_SER_ERRO_GETPORID)); 
        }finally{
            em.close();
        }
    }
    
}
