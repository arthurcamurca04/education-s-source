
package br.com.education.source.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Nota implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String bimestre;
    
    private Double mediaBim01;
    
    private Double mediaBim02;
    
    private Double mediaBim03;
    
    private Double mediaBim04;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public Double calcularMediaGlobal(){
        return null;
        
    }
    public String situacaoAluno(int matricula){
        return null;
    }

    public String getBimestre() {
        return bimestre;
    }

    public void setBimestre(String bimestre) {
        this.bimestre = bimestre;
    }

    public Double getMediaBim01() {
        return mediaBim01;
    }

    public void setMediaBim01(Double mediaBim01) {
        this.mediaBim01 = mediaBim01;
    }

    public Double getMediaBim02() {
        return mediaBim02;
    }

    public void setMediaBim02(Double mediaBim02) {
        this.mediaBim02 = mediaBim02;
    }

    public Double getMediaBim03() {
        return mediaBim03;
    }

    public void setMediaBim03(Double mediaBim03) {
        this.mediaBim03 = mediaBim03;
    }

    public Double getMediaBim04() {
        return mediaBim04;
    }

    public void setMediaBim04(Double mediaBim04) {
        this.mediaBim04 = mediaBim04;
    }
    
    
}
