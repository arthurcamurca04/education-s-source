
package br.com.education.source.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Administrador extends Pessoa implements Serializable{
   
    @Column(unique = true)
    private Long cpfAdmin;    
    
    public void realizarLogin(int cpfAdmin,String senha){
        
    }
    public void realizarCadastroProf(Professor professor){
        
    }
    public void excluirCadastroProf(int cpf){
        
    }
    public void alterarCadastroProf(int cpf){
        
    }
    public Professor consultarProfessor(int cpf){
        return null;
    }
    public List<Professor> listarProfessores(){
        return null;
    }
    public void realizarCadastroAluno(Aluno aluno){
        
    }
    public void excluirCadastroAluno(String matricula){
        
    }
    public void alterarCadastroAluno(String matricula){
        
    }
    public Aluno consultarAluno(String matricula){
        return null;
    }
    public List<Aluno> listarAluno(){
        return null;
    }
    public void cadastrarDiciplina(Disciplina diciplina){
        
    }
    public void excluirDiciplina(int codDic){
        
    }
    public void alterardiciplina(int codDisc){
        
    }
    public Disciplina consultarDiciplina(int codDisc){
        return null;
        
    }
    public List<Disciplina> listarDiciplinas(){
        return null;
    }

    public Long getCpfAdmin() {
        return cpfAdmin;
    }

    public void setCpfAdmin(Long cpfAdmin) {
        this.cpfAdmin = cpfAdmin;
    }
    
    
}
