package br.com.education.source.model;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity
public class Professor extends Pessoa{
    
    @Column(unique = true)
    private String cpf;
    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Disciplina> disciplinas;

    public Professor() {
        
    }
    @Override
    public String toString() {
        return "Professor" + "\nCPF: " + cpf + "\nDisciplinas lecionadas: " + disciplinas;
                
    }
    public List<Aluno> listarAluno() {
        return null;
    }

    public List<Disciplina> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }    

    public void realizarLogin(int cpf, String senha) {

    }

    public void registrarFreguencia(Aluno aluno) {

    }

    public void atribuirNotas(Aluno aluno, Nota nota) {

    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public List<Disciplina> getDiciplinas() {
        return disciplinas;
    }

    public void setDiciplinas(List<Disciplina> diciplinas) {
        this.disciplinas = diciplinas;
    }

}
