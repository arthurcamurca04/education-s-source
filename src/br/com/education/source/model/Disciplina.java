
package br.com.education.source.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Disciplina implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int codDisc;
    
    private String nomeDisc;    
    private String horrario;
    @OneToOne
    private Nota notas;

    public int getCodDisc() {
        return codDisc;
    }

    public void setCodDisc(int codDisc) {
        this.codDisc = codDisc;
    }

    public String getNomeDisc() {
        return nomeDisc;
    }

    public void setNomeDisc(String nomeDisc) {
        this.nomeDisc = nomeDisc;
    }

    public String getHorrario() {
        return horrario;
    }

    public void setHorrario(String horrario) {
        this.horrario = horrario;
    }

    public Nota getNotas() {
        return notas;
    }

    public void setNotas(Nota notas) {
        this.notas = notas;
    }
    
    
}
