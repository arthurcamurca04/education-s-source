package br.com.education.source.model;

import br.com.education.source.util.DateUtil;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Aluno extends Pessoa {

    @Column(unique = true)
    private String matricula;        
    private String responsavel;
    private String nomeDoPai;
    private String nomeDaMae;    

    public Aluno() {
        
    }

    @Override
    public String toString() {
        return "Aluno" + "\nMatricula: " + matricula + "\nResponsavel: " + responsavel + 
                "\nPai: " + nomeDoPai + "\nMãe: " + nomeDaMae + 
                "\nData de Nascimento: " + DateUtil.dateToString(getDataNascimento());
    }    

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    public String getNomeDoPai() {
        return nomeDoPai;
    }

    public void setNomeDoPai(String nomeDoPai) {
        this.nomeDoPai = nomeDoPai;
    }

    public String getNomeDaMae() {
        return nomeDaMae;
    }

    public void setNomeDaMae(String nomeDaMae) {
        this.nomeDaMae = nomeDaMae;
    }

    public void realizarLogin(int matricula, String senha) {

    }

    public List<Nota> consultarNota(Disciplina diciplina) {
        return null;
    }

    public String visualizarHistorico() {
        return "";

    }

    public String visualizarCalendarioEscolar() {
        return "";
    }

}
